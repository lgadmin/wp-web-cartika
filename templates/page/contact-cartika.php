<?php

get_header(); ?>

<?php get_template_part( '/templates/template-parts/page/feature-image' ); ?>

<div id="primary">
	<div id="content" role="main" class="site-content">
		<main>
			
			<div class="container py-5">
				<div class="row align-items-top">
					<div class="col col-12 col-md-6 d-flex flex-column">
						<h3>TELEPHONE NUMBERS</h3>
						<a href="tel:<?php echo format_phone(do_shortcode('[lg-phone-main]')); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?> (toll-free)</a>
						<?php
							if( have_rows('locations', 'option') ):
							    while ( have_rows('locations', 'option') ) : the_row();
							        $phone = get_sub_field('phone');
							        $name = get_sub_field('name');
							        ?>
							        <a href="<?php echo format_phone($phone); ?>"><?php echo format_phone($phone); ?> (<?php echo strtolower($name); ?> local)</a>
							        <?php
							    endwhile;
							else :
							    // no rows found
							endif;
							?>

						<?php
						if( have_rows('locations', 'option') ):
							?>
							<h3 class="mt-2 mb-0">MAILING ADDRESSES</h3>
							<?php
						    while ( have_rows('locations', 'option') ) : the_row();
						        $name = get_sub_field('name');
						        $address = get_sub_field('address');
						        $city = get_sub_field('city');
						        $province = get_sub_field('province');
						        $postcode = get_sub_field('postcode');
						        $country = get_sub_field('country');
						        ?>
						        <section class="mt-2">
						        	<div><strong>Cartika <?php echo $name; ?></strong></div>
						        	<?php if($address): ?>
						        		<?php echo $address; ?><br>
						        	<?php endif; ?>

						        	<?php if($city && $province == ''): ?>
						        		<?php echo $city; ?><br>
						        	<?php elseif($city && $province): ?>
						        		<?php echo $city; ?>, <?php echo $province; ?><br>
						        	<?php endif; ?>

						        	<?php if($postcode): ?>
						        		<?php echo $postcode; ?><br>
						        	<?php endif; ?>

						        	<?php if($country): ?>
						        		<?php echo $country; ?>
						        	<?php endif; ?>
						        </section>
						        <?php
						    endwhile;
						else :
						    // no rows found
						endif;
						?>
					</div>
					<div class="col col-12 col-md-6 d-flex flex-column">
						<h3>SEND A MESSAGE</h3>
						<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
					</div>
				</div>
			</div>

			<?php get_template_part( '/templates/template-parts/flexible-components/cta-flexible' ); ?>

		</main>
	</div>
</div>

<?php get_footer(); ?>