<?php

get_header(); ?>

<?php get_template_part( '/templates/template-parts/page/feature-image' ); ?>

<div id="primary">
	<div id="content" role="main" class="site-content">
		<main>

			<div class="py-5 container">

				<?php
					$args = array(
				        'showposts'	=> -1,
				        'post_type'		=> 'white-papers',
				    );

				    $result = new WP_Query( $args );

				    // Loop
				    if ( $result->have_posts() ) :?>
						
				    	<?php
				        while( $result->have_posts() ) : $result->the_post(); 
						$landing_page_subtitle = get_field('landing_page_subtitle');
						$landing_page_short_description = get_field('landing_page_short_description');
						$landing_page_image = get_field('landing_page_image');
						$link = get_permalink();
				    	?>
				    	<div class="white-papers row">
					        <div class="col-md-4 col-lg-3">
								<img class="img-full mb-4 mb-md-0" src="<?php echo $landing_page_image['url']; ?>" alt="<?php echo $landing_page_image['alt']; ?>">
							</div>
							<div class="col-md-8 col-lg-9">
								<h2 class="h3 text-primary">
									<?php echo $landing_page_subtitle; ?>
								</h2>
								<?php echo $landing_page_short_description; ?>
								<a href="<?php echo $link; ?>" class="btn btn-blue">Learn More</a>
							</div>
						</div>
						<hr />
						<?php
				        endwhile;
				        ?>
				        
				        
					<?php
				    endif; // End Loop

				    wp_reset_query();
				?>
			</div>

		</main>
	</div>
</div>

<?php get_footer(); ?>

