<?php 

	$featureimage = get_field('feature_image'); 
	$feature_image_half_image = get_field('feature_image_half_image');

?>



<?php if(is_front_page()): ?>
	<section id="cartika-featureimage" class="py-5 bg-primary text-white" style="background-image:url(<?php echo $featureimage['url']; ?>)">
		<div class="container"> 
			<div class="row align-items-center justify-content-between">
				<div class="feature_image_half_content col-xs-12 col-md-6">
					<?php the_field('feature_image_content'); ?>
				</div>
				<div class="feature_image_half_image col-xs-12 col-md-6">
					<img src="<?php echo $feature_image_half_image['url']; ?>" alt="<?php echo $feature_image_half_image['alt']; ?>">
				</div>
			</div>
		</div>
	</section>
<?php else: ?>
	<section id="cartika-featureimage" class="py-5 bg-primary text-white" style="background-image:url(<?php echo $featureimage['url']; ?>)">
		<div class="container"> 
			<div class="row align-items-center justify-content-center">
				<div class="feature_image_half_content col text-center">
					<?php the_field('feature_image_content'); ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>