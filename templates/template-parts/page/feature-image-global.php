<?php 
	$heading_content = get_field('heading_content', 'option');
?>

<section id="cartika-featureimage" class="py-5 bg-primary text-white" style="background-image:url(<?php echo $featureimage['url']; ?>)">
	<div class="container"> 
		<div class="row align-items-center justify-content-center">
			<div class="feature_image_half_content col text-center">
				<?php echo $heading_content ?>
			</div>
		</div>
	</div>
</section>