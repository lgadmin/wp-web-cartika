<?php

	$content = get_field('buy_now_content');
	$button = get_field('buy_now_button');

?>

<div class="buy-now py-5 text-center bg-primary text-white">
	<div><?php echo $content; ?></div>
	<?php if($button): ?>
		<div class="buttons">
			<a class="btn btn-secondary" href="<?php echo $button['url']; ?>"><?php echo $button['title']; ?></a>
		</div>
	<?php endif; ?>
</div>