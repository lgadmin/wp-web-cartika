<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		
		<article class="col">
			
			<header>
				<h2 class="mb-0"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p class="small font-italic">&nbsp;<?php the_date('F d,  Y')  ?> </p>
			</header>

			<div class="article-content">
				<p><?php the_content(); ?></p>
			</div>

		</article>

		<hr>

	<?php endwhile; ?>
<?php endif ?>