<?php 
/**
 * General Text block component.
 *
 */
?>

<?php 
	
	// Background Colours
	$background_color = get_sub_field('background_colour'); 
	$background_image = $background_color['background_image'];
	
	if ( $background_color['background_image'] && $background_color['background_colour'] == 'bg-image') {
		$background_image = ' style="background-image:url(' . $background_color['background_image'] . ')" '; 
	} 

	// Padding & Margin
	$block_padding = get_sub_field('block_padding');
	$block_margin  = get_sub_field('block_margin'); 
	$item_padding  = $block_padding['padding'] . '-' . $block_padding['size'];
	$item_margin   = $block_margin['margin'] . '-' . $block_margin['size'];


	// Block Fields
	$buttons = get_sub_field('buttons');
	$block_title = get_sub_field('block_title');
	$items_per_row = get_sub_field('items_per_row');
	$items_padding = get_sub_field('items_padding');
?> 


<section class="flexible-item grid-layout <?php echo $class; ?> <?php the_sub_field('custom-classes'); ?> <?php echo $background_color['background_colour']; ?> <?php echo $item_margin; ?>" <?php echo $background_image; ?> >
	<div class="<?php the_sub_field('container'); ?>  <?php echo $item_padding; ?>">
		<?php if(get_sub_field('block_title_show') == 1): ?>
			<h2 class="text-center mb-4"><?php echo $block_title; ?></h2>
		<?php endif; ?>
		<div class="row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
			<?php if($buttons && is_array($buttons)): ?>
				<div class="buttons">
				<?php foreach ($buttons as $key => $button): ?>
					<div class="col-sm-6 col-md-<?php echo $items_per_row[0]; ?> item-padding-none text-center" <?php if($items_padding): ?>style="padding: <?php echo $items_padding; ?>"<?php endif; ?>>
						<?php if($button['image']): ?>
						<img src="<?php echo $button['image']['url']; ?>" alt="<?php echo $button['image']['alt']; ?>"><?php endif; ?>
					<a class="btn btn-primary" href="<?php echo $button['button']['url']; ?>"><?php echo $button['button']['title']; ?></a>
					</div>
				<?php endforeach; ?>
				</div>
			<?php endif; ?>

		</div>
	</div>
</section>