<?php 
/**
 * General Text block component.
 *
 */
?>

<?php 
	
	// Background Colours
	$background_color = get_sub_field('background_colour'); 
	$background_image = $background_color['background_image'];
	
	if ( $background_color['background_image'] && $background_color['background_colour'] == 'bg-image') {
		$background_image = ' style="background-image:url(' . $background_color['background_image'] . ')" '; 
	} 

	// Padding & Margin
	$block_padding = get_sub_field('block_padding');
	$block_margin  = get_sub_field('block_margin'); 
	$item_padding  = $block_padding['padding'] . '-' . $block_padding['size'];
	$item_margin   = $block_margin['margin'] . '-' . $block_margin['size'];

	$block_title = get_sub_field('block_title');

?> 


<section class="flexible-item <?php the_sub_field('custom-classes'); ?> <?php echo $background_color['background_colour']; ?> <?php echo $item_margin; ?>" <?php echo $background_image; ?> >
	<div class="<?php the_sub_field('container'); ?>  <?php echo $item_padding; ?>">
		<?php if(get_sub_field('block_title_show') == 1): ?>
			<h2 class="text-center mb-4 container"><?php echo $block_title; ?></h2>
		<?php endif; ?>
		
		<!-- partners -->
		<?php if( have_rows('partners') ): ?>
			    <ul class='partners'>
			  	 <?php while ( have_rows('partners') ) : the_row(); ?>
			        <li class=" single-partner d-flex flex-column justify-content-between align-items-start">
			            <?php
			            	$partner_id = get_sub_field('partner');
			            	$partner_feature_content = get_sub_field('partner_feature_content');
			            	$logo = get_field('logo', $partner_id);
			            	$sub_title = get_field('sub_title', $partner_id);
			            	$title = get_the_title($partner_id);
			            	$link = get_field('link', $partner_id);
			            	$url = get_permalink($partner_id);
			            	$right = get_field('right', $partner_id);
			            	$website_url = $right['website_url'];
			            ?>

			            <div>
			            	<div class="logo mb-3">
			            		<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
			            	</div>
			            	<div class="title">
			            		<h4 class="mb-0"><?php echo $sub_title; ?></h4>
			            		<h2 class="text-body h1 text-uppercase"><?php echo $title; ?></h2>
			            	</div>

				            <div class="mt-2 mb-4">
				            	<p><?php echo $partner_feature_content; ?></p>
				            </div>
			            </div>

			            <?php if($link == 'Learn More'): ?>
			            	<a href="<?php echo $url; ?>" class="btn btn-primary">LEARN MORE</a>
			            <?php elseif($link == 'Visit'): ?>
			            	<a href="<?php echo $website_url; ?>" class="btn btn-primary">VISIT</a>
			            <?php endif; ?>
			        </li>
			    <?php endwhile; ?>
			    </ul>
			<?php endif; ?>
		<!-- end partners -->
	</div>
</section>