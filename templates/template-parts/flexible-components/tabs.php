<?php 
/**
 * General Text block component.
 *
 */
?>

<?php 
	$i = 0;

	// Background Colours
	$background_color = get_sub_field('background_colour'); 
	$background_image = $background_color['background_image'];
	
	if ( $background_color['background_image'] && $background_color['background_colour'] == 'bg-image') {
		$background_image = ' style="background-image:url(' . $background_color['background_image'] . ')" '; 
	} 

	// Padding & Margin
	$block_padding = get_sub_field('block_padding');
	$block_margin  = get_sub_field('block_margin'); 
	$item_padding  = $block_padding['padding'] . '-' . $block_padding['size'];
	$item_margin   = $block_margin['margin'] . '-' . $block_margin['size'];

	  
?> 


<section class="flexible-item <?php the_sub_field('custom-classes'); ?> <?php echo $background_color['background_colour']; ?> <?php echo $item_margin; ?>" <?php echo $background_image; ?> >
	<div class="<?php the_sub_field('container'); ?>  <?php echo $item_padding; ?>">
		<div class="row <?php the_sub_field('align_items_vertical'); ?>">
			<div class="col col-12">
			
				<?php if( have_rows('tabs') ): ?>
				  <div class="tab-content">
					<?php while(has_sub_field('tabs')): ?>

						<?php     
							$tabID = get_sub_field('tab_button_label');
						    $tabID = str_replace(" ", "-", $tabID); 
						    $tabID = strtolower($tabID);
						 ?>

					    <div id="<?php echo 'self-' . $tabID; ?>" class="tab-pane fade <?php if ($i == 0) { echo 'active show'; } ?>" aria-labelledby="<?php echo 'self-' . $tabID; ?>">
					    	<?php the_sub_field('tab_content'); ?>
						</div>
						<?php $i++; ?>
					<?php endwhile; ?>
				  </div>
				<?php endif; ?>
			    
			</div>
			<div class="col col-12">
				<?php if( have_rows('tabs') ): ?>
					<ul class="nav nav-pills justify-content-center mt-3">
						<?php while(has_sub_field('tabs')): ?>
						
						<?php     
							$tabID = get_sub_field('tab_button_label');
						    $tabID = str_replace(" ", "-", $tabID); 
						    $tabID = strtolower($tabID);
						 ?>

						    <li class="nav-item">
						    	<a class="nav-link" id="<?php echo 'self-' . $tabID; ?>" data-toggle="pill" href="#<?php echo 'self-' . $tabID; ?>" ><?php the_sub_field('tab_button_label'); ?></a>
						    </li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			 
			</div>
		</div>
	</div>
</section>
