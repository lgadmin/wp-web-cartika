<?php if( have_rows('cta') ):
	while ( have_rows('cta') ) : the_row();
		
		switch ( get_row_layout()) {

			// Alpha ~ Regular text block
			case 'cta_alpha':
				get_template_part('/templates/template-parts/flexible-components/text-block');
			break;
			
			// Half and Half Content Block
			case 'half-half':
				get_template_part('/templates/template-parts/flexible-components/half-half');
			break;
			
			// 8 cols on the left - 4 cols on the right
			case '8col-4col':
				get_template_part('/templates/template-parts/flexible-components/8col-4col');
			break;
			
			// TABS Navigation
			case 'tabs_navigation':
				get_template_part('/templates/template-parts/flexible-components/tabs');
			break;

			// Icon Navigation
			case 'icon_navigation':
				get_template_part('/templates/template-parts/flexible-components/nav-icon');
			break;

			default:
				echo "<!-- nothing to see here -->";
			break;
		}

	endwhile; else : // no layouts found 
endif; ?>