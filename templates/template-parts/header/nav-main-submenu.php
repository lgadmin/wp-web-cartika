<!--<nav class="main-sub-nav">
	<?php 

		//	  https://christianvarga.com/how-to-get-submenu-items-from-a-wordpress-menu-based-on-parent-or-sibling/
		wp_nav_menu( array(
		  'menu'     => 'main',
		  'sub_menu' => true,
		) );

	?>
</nav>
-->


<?php

	$post_type = get_post_type();
    $category = wp_get_post_terms($post->ID, $post_type.'-category');

    if($category && $category[0]){
        $category = $category[0];
    }else{
        $category = '';
    }

	$slug = $post->post_name;

	$args = array(
        'showposts'	=> -1,
        'post_type'	=> $post_type,
        'tax_query' => array(
            array(
                'taxonomy' => $post_type.'-category',
                'field'    => 'slug',
                'terms'    => $category,
                'include_children'  => false
            ),
        ),
    );

    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
    	
		<div class="sub-nav-items py-2">
    	<?php
        while( $result->have_posts() ) : $result->the_post(); 
        $icon = get_field('icon');
        $title = get_the_title();
        $link = get_permalink();
    ?>
        <div class="sub-nav-item <?php if($slug == $post->post_name){echo 'active';} ?>">
        	<a href="<?php echo $link; ?>" class="icon">
        		<div style="background-image: url('<?php echo $icon; ?>');"></div>
        	</a>
        	<a href="<?php echo $link; ?>" class="title">
        		<div class="sub-nav-item-title"><?php echo $title; ?></div>
        	</a>
        </div>

		<?php
        endwhile;
        ?>
        </div>
        <?php
    endif; // End Loop

    wp_reset_query();

?>