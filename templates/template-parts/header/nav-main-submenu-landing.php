
<?php if ( have_posts() ) : ?>
	<div class="sub-nav-items py-2">
	<?php while ( have_posts() ) : the_post(); 
        $icon = get_field('icon');
        $title = get_the_title();
        $link = get_permalink();
    ?>
    <div class="sub-nav-item <?php if($slug == $post->post_name){echo 'active';} ?>">
    	<a href="<?php echo $link; ?>" class="icon">
    		<div style="background-image: url('<?php echo $icon; ?>');"></div>
    	</a>
    	<a href="<?php echo $link; ?>" class="title">
    		<div class="sub-nav-item-title"><?php echo $title; ?></div>
    	</a>
    </div>

	<?php
    endwhile;
    ?>
    </div>
    <?php
endif; // End Loop

wp_reset_query();

?>