<nav class="navbar navbar-expand-md navbar-dark">
  <!-- <a class="navbar-brand" href="#">Navbar</a> -->
  <div class="main-nav py-1 py-md-0 col">
  		<?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-main" aria-controls="nav-main" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
  </div>

	<?php
		wp_nav_menu( array(
			'theme_location'    => 'top-nav',
			'depth'             => 3,
			'container'         => 'div',
			'container_class'   => 'collapse navbar-collapse',
			'container_id'      => 'nav-main',
			'menu_class'        => 'nav navbar-nav ml-auto px-3 px-md-0 py-3 py-md-0',
			'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			'walker'            => new WP_Bootstrap_Navwalker(),
		) );
	?>

</nav>