
<div class="utility-bar bg-night text-white">
  <div class="container-fluid">
    <div class="row justify-content-end">
      <div class="col">
        <?php wp_nav_menu( array(
          'theme_location'  => 'utilitybar',
          'container'       => 'nav',
          'container_class' => 'menu-utilitybar-container',
          'menu_class'      => 'nav justify-content-end',
        ) ); ?>
      </div>
    </div>
  </div>
</div>