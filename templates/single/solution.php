<?php
get_header(); ?>

<?php get_template_part( '/templates/template-parts/page/feature-image' ); ?>

<section class="bg-light-dark">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php get_template_part('/templates/template-parts/header/nav-main-submenu'); ?>
			</div>
		</div>
	</div>
</section>

<div id="primary">
	<div id="content" role="main" class="site-content">
		<main>
			
			<?php get_template_part( '/templates/template-parts/flexible-components/cta-flexible' ); ?>

			<?php if(get_field('services') || get_field('partners')): ?>
			<hr class="my-0 container">

			<div class="py-5 container">

				<!-- Services -->

				<?php

					$posts = get_field('services');

					if( $posts ): ?>
					    <ul class="services">
					    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
					        <?php setup_postdata($post); ?>
					        <li>
					        	<?php
					        		$solution_featured_image = get_field('solution_featured_image');
					        		$short_label = get_field('short_label');
					        		$short_intro = get_field('short_intro');
					        		$url = get_permalink();
					        		$title = get_the_title();
					        	?>
					        	<div class="row">
					        		<div class="col-12 col-md-6 order-2 order-md-1">
						        		<h2 class="text-body h1 text-uppercase service-heading"><?php echo $title; ?></h2>
						        		<?php if($short_label): ?>
							        		<h3><?php echo $short_label; ?></h3>
							        	<?php endif; ?>

							            <div class="mt-2 mb-4">
							            	<p><?php echo $short_intro; ?></p>
							            </div>

							            <a href="<?php echo $url; ?>" class="btn btn-primary">LEARN MORE</a>
					        		</div>
					        		<div class="col-12 col-md-6 order-1 order-md-2">
					        			<img class="mb-3" src="<?php echo $solution_featured_image['url']; ?>" alt="<?php echo $solution_featured_image['alt']; ?>">
					        		</div>
					        	</div>
					        </li>
					    <?php endforeach; ?>
					    </ul>
					    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; ?>

				<!-- Partners -->

				<?php
				
					$posts = get_field('partners');

					if( $posts ): ?>
					    <ul class='partners'>
					    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
					        <?php setup_postdata($post); ?>
					        <li class="single-partner d-flex flex-column justify-content-between align-items-start">
					            <?php
					            	$logo = get_field('logo');
					            	$sub_title = get_field('sub_title');
					            	$title = get_the_title();
					            	$short_intro = get_field('short_intro');
					            	$link = get_field('link');
					            	$url = get_permalink();
					            	$right = get_field('right');
					            	$website_url = $right['website_url'];
					            ?>

								<div>
					            	<div class="logo mb-3">
				            			<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
					            	</div>
					            	<div class="title">
					            		<h4 class="mb-0"><?php echo $sub_title; ?></h4>
					            		<h2 class="text-body h1 text-uppercase"><?php echo $title; ?></h2>
					            	</div>

						            <div class="mt-2 mb-4">
						            	<p><?php echo $short_intro; ?></p>
						            </div>
								</div>

					            <?php if($link == 'Learn More'): ?>
					            	<a href="<?php echo $url; ?>" class="btn btn-primary">LEARN MORE</a>
					            <?php elseif($link == 'Visit'): ?>
					            	<a href="<?php echo $website_url; ?>" class="btn btn-primary">VISIT</a>
					            <?php endif; ?>
					        </li>
					    <?php endforeach; ?>
					    </ul>
					    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; ?>


			</div>

			<?php endif; ?>

		</main>
	</div>
</div>

<?php get_footer(); ?>