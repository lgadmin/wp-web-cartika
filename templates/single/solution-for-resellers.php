<?php

get_header(); ?>

<?php get_template_part( '/templates/template-parts/page/feature-image' ); ?>

<section class="bg-light-dark">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php get_template_part('/templates/template-parts/header/nav-main-submenu'); ?>
			</div>
		</div>
	</div>
</section>

<div id="primary">
	<div id="content" role="main" class="site-content">
		<main>

			<?php get_template_part( '/templates/template-parts/flexible-components/cta-flexible' ); ?>

		</main>
	</div>
</div>

<?php get_footer(); ?>