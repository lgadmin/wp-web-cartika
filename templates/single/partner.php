<?php

get_header(); ?>

<?php get_template_part( '/templates/template-parts/page/feature-image' ); ?>

<div id="primary">
	<div id="content" role="main" class="site-content">
		<main>

			<?php
				$logo = get_field('logo');
				$sub_title = get_field('sub_title');
				$title = get_the_title();

				$left = get_field('left');
				$partner_feature_image = $left['partner_feature_image'];
				$content = $left['content'];

				$right = get_field('right');
				$website_url = $right['website_url'];
				$learn_more_url = $right['learn_more_url'];
				$additional_content = $right['additional_content'];
				$contact = $right['contact'];
				$testimonials = $right['testimonials'];
			?>

			<div class="container py-5">
				<div>
					<h3 class="sub-title h4 mb-0 text-primary text-uppercase"><?php echo $sub_title; ?></h3>
					<h2 class="h1 text-uppercase"><?php echo $title; ?></h2>
				</div>
				
				<div class="row">
					<div class="col-12 col-md-6 mt-3">
						<img class="mb-3" src="<?php echo $partner_feature_image['url']; ?>" alt="<?php echo $partner_feature_image['alt']; ?>">
						<?php echo $content; ?>
					</div>

					<div class="col-12 col-md-6 mt-3">
						<div class="border py-3">
							<div class="text-center"><img class="my-5 mx-auto" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"></div>

							<a class="bg-secondary d-flex py-3 px-4 text-white justify-content-center" href="<?php echo $website_url; ?>"><strong class="h3 mb-0">VISIT WEBSITE</strong></a>

							<div class="d-flex justify-content-center">
								<a class="btn btn-primary text-white my-4" href="<?php echo $learn_more_url; ?>">Learn More</a>
							</div>

							<div class="justify-content-center px-4">
								<h2 class="text-color text-center">Why <?php echo $title; ?> Partners with Cartika</h2>

								<?php
								if( $testimonials && is_array($testimonials) ):
								    foreach ($testimonials as $key => $testimonial):
								        $testimonial_author = get_sub_field('testimonial_author');
								        $testimonial_content = get_sub_field('testimonial_content');
								        ?>
								        <section>
								        	<blockquote>
								        		<?php echo $testimonial['testimonial_content']; ?>
								        	</blockquote>
								        	<p class="author"><strong><?php echo $testimonial['testimonial_author']; ?></strong></p>
								        </section>
								        <?php
								    endforeach;
								else :
								    // no rows found
								endif;
								?>

								<?php if($additional_content): ?>
									<div class="my-3">
										<?php echo $additional_content; ?>
									</div>
								<?php endif; ?>

								<?php if($contact): ?>
									<strong>Contact</strong>
									<div class="mb-3 mt-1">
										<a href="mailto:<?php echo $contact; ?>"><?php echo $contact; ?></a>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>

		</main>
	</div>
</div>

<?php get_footer(); ?>