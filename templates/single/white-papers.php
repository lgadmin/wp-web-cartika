<?php

get_header(); ?>

<?php get_template_part( '/templates/template-parts/page/feature-image' ); ?>

<div id="primary">
	<div id="content" role="main" class="site-content">
		<main>

			<?php
				$content_main = get_field('content_main');
				$additional_content = get_field('additional_content');
			?>

			<div class="py-5 container">
				<div class="row">
					<div class="col-md-5 mb-4 mb-md-0">
						<img class="mb-3"  src="<?php echo $content_main['image']['url']; ?>" alt="<?php echo $content_main['image']['alt']; ?>">
						<?php echo $content_main['content']; ?>
					</div>
					<div class="col-md-7">
						<iframe id="ssf_M7A0tLQ0N03UNU2zMNQ1STY107WwTDLWNUpKSUpNMU82TTO1BAA" src="https://app-3QN7TAQVEW.marketingautomation.services/prospector/form/MzawMDE1tTQ3AAA/M7A0tLQ0N03UNU2zMNQ1STY107WwTDLWNUpKSUpNMU82TTO1BAA?rf__sb=https%3A%2F%2Fwww.cartika.com%2Fcartika-whitepapers%2F&_tk=201809|5babbefa235c9506bd0cccf2" style="overflow-y: auto" frameborder="0" height="369" width="100%"></iframe>
					</div>
				</div>
			</div>

			<div class="py-4 bg-primary text-white">
				<div class="container">
					<?php echo $additional_content; ?>
				</div>
			</div>

		</main>
	</div>
</div>

<?php get_footer(); ?>