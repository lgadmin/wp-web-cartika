<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

<section id="cartika-featureimage" class="py-5 bg-primary text-white">
	<div class="container"> 
		<div class="row align-items-center justify-content-center">
			<div class="feature_image_half_content col text-center">
				<?php if(is_tax()): ?>
					<h1 class="mb-0 text-uppercase"><?php echo single_cat_title('', false); ?></h1>
				<?php else: ?>
					<h1 class="mb-0 text-uppercase"><?php echo post_type_archive_title('', false); ?></h1>
				<?php endif; ?>
				
			</div>
		</div>
	</div>
</section>

<section class="bg-light-dark">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php get_template_part('/templates/template-parts/header/nav-main-submenu-landing'); ?>
			</div>
		</div>
	</div>
</section>

<div id="primary">
	<div id="content" role="main" class="site-content landing-page">
		<main>

			<?php if(get_the_content()): ?>
				<div class="py-5"><?php echo get_the_content(); ?></div>
			<?php endif; ?>
			
			   <?php if ( have_posts() ) : 
			   	$index = 1;
			   	?>
				<?php while ( have_posts() ) : the_post();
			        $icon = get_field('icon');
			        $title = get_the_title();
			        $subtitle = get_field('landing_page_subtitle');
			        $image = get_field('landing_page_image');
			        $short_description = get_field('landing_page_short_description');
			        $link = get_permalink();

					 ?>
			       <div class="py-5 <?php if($index % 2 == 0){ echo 'bg-light'; } ?>">
			       		<div class="container">
			       			<div class="split-content">
			       				<div class="split-image">
			       					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
			       				</div>
			       				<div class="split-copy">
			       					<h2 class="h1 text-default bg-icon" style="background-image: url('<?php echo $icon; ?>');">
			       						<?php echo $title; ?>
			       					</h2>
			       					<?php if($subtitle): ?>
				       					<h3><?php echo $subtitle; ?></h3>
				       				<?php endif; ?>
				       				<div class="py-2">
				       					<?php echo $short_description; ?>
				       				</div>
				       				<a href="<?php echo $link; ?>" class="btn btn-blue">LEARN MORE</a>
			       				</div>
			       			</div>
			       		</div>
			       </div>

					<?php
					$index++;
			        endwhile;
			    endif; // End Loop
			?>

		</main>
	</div>
</div>

<?php get_footer(); ?>