<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

<div id="primary">

	<?php get_template_part( '/templates/template-parts/page/feature-image-global' ); ?>

	<main id="content" role="main" class="site-content pt-5">
		<div class="body-copy">
			<?php get_template_part( 'templates/template-parts/content/content-loop'); ?>
			<div class="h3 col">
			<?php
				the_posts_pagination( array(
					'mid_size'  => 5,
					'prev_text' => '',
					'next_text' => '',
					'screen_reader_text' => ' ',
				) );
			?>
			</div>
		</div>

		<?php get_sidebar(); ?>
	</main>
</div>

<?php get_footer(); ?>

