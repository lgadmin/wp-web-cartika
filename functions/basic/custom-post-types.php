<?php

//Custom Post Types
function create_post_type() {

    // SERVICE
    
    register_post_type( 'service',
        array(
          'labels' => array(
            'name' => __( 'Services' ),
            'singular_name' => __( 'Service' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          "rewrite" => array("with_front" => false, "slug" => 'services'),
          'show_in_menu'    => 'cartika',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // SOLUTION
    
    register_post_type( 'solution',
        array(
          'labels' => array(
            'name' => __( 'Partner Solutions' ),
            'singular_name' => __( 'Partner Solution' )
          ),
          'public' => true,
          'has_archive' => true,
          'menu_icon'   => 'dashicons-location',
          "rewrite" => array("slug" => 'partners', "with_front" => false),
          'show_in_menu'    => 'cartika',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' ),
        )
    );

    // PARTNER
    
    register_post_type( 'partner',
        array(
          'labels' => array(
            'name' => __( 'Partners' ),
            'singular_name' => __( 'Partner' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          "rewrite" => array("with_front" => false),
          'show_in_menu'    => 'cartika',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // WHITE PAPER
    
    register_post_type( 'white-papers',
        array(
          'labels' => array(
            'name' => __( 'White Papers' ),
            'singular_name' => __( 'White Papers' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          "rewrite" => array("with_front" => false),
          'show_in_menu'    => 'cartika',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

}
add_action( 'init', 'create_post_type' );

?>