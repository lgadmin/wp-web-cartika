<?php

function create_taxonomy(){

	register_taxonomy(
		'service-category',
		'service',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( "with_front" => false, 'slug' => 'services' ),
			'hierarchical' => true,
		)
	);

	register_taxonomy(
		'solution-category',
		'solution',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( "with_front" => false, 'slug' => 'partners' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );

?>