<?php

// Admin Screen Filters

add_action('restrict_manage_posts', 'service_filter_by_category');

function service_filter_by_category() {
  global $typenow;
  $post_type = 'service'; // change to your post type
  $taxonomy  = 'service-category'; // change to your taxonomy
  if ($typenow == $post_type) {
    $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
    $info_taxonomy = get_taxonomy($taxonomy);
    wp_dropdown_categories(array(
      'show_option_all' => __("Show All {$info_taxonomy->label}"),
      'taxonomy'        => $taxonomy,
      'name'            => $taxonomy,
      'orderby'         => 'name',
      'selected'        => $selected,
      'show_count'      => true,
      'hide_empty'      => true,
      'hierarchical'    => false
    ));
  };
}
  
add_filter('parse_query', 'tsm_convert_id_to_term_in_query_service');
function tsm_convert_id_to_term_in_query_service($query) {
  global $pagenow;
  $post_type = 'service'; // change to your post type
  $taxonomy  = 'service-category'; // change to your taxonomy
  $q_vars    = &$query->query_vars;
  if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
    $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
    $q_vars[$taxonomy] = $term->slug;
  }
}

add_action('restrict_manage_posts', 'solution_filter_by_category');

function solution_filter_by_category() {
  global $typenow;
  $post_type = 'solution'; // change to your post type
  $taxonomy  = 'solution-category'; // change to your taxonomy
  if ($typenow == $post_type) {
    $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
    $info_taxonomy = get_taxonomy($taxonomy);
    wp_dropdown_categories(array(
      'show_option_all' => __("Show All {$info_taxonomy->label}"),
      'taxonomy'        => $taxonomy,
      'name'            => $taxonomy,
      'orderby'         => 'name',
      'selected'        => $selected,
      'show_count'      => true,
      'hide_empty'      => true,
      'hierarchical'    => false
    ));
  };
}

add_filter('parse_query', 'tsm_convert_id_to_term_in_query_solution');
function tsm_convert_id_to_term_in_query_solution($query) {
  global $pagenow;
  $post_type = 'solution'; // change to your post type
  $taxonomy  = 'solution-category'; // change to your taxonomy
  $q_vars    = &$query->query_vars;
  if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
    $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
    $q_vars[$taxonomy] = $term->slug;
  }
}


function main_nav_items ( $items, $args ) {
    if ($args->menu->slug == 'utility') {
        $items = '<li class="menu-item menu-item-type-custom menu-item-object-custom"><a style="font-weight:600;" href="tel:'.do_shortcode('[lg-phone-main]').'">'.format_phone(do_shortcode('[lg-phone-main]')).'</a></li>' . $items;
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );

// Add our custom permastructures for custom taxonomy and post
add_action( 'wp_loaded', 'add_clinic_permastructure' );
function add_clinic_permastructure() {
  global $wp_rewrite;
  add_permastruct( 'service', 'services/%service-category%/%service%', false );
}
// Make sure that all links on the site, include the related texonomy terms
add_filter( 'post_type_link', 'service_permalinks', 10, 2 );
function service_permalinks( $permalink, $post ) {
  if ( $post->post_type !== 'service' ){
    return $permalink;
  }else{
    $terms = get_the_terms( $post->ID, 'service-category' );
      if ( ! $terms ){
        write_log('1');
        return str_replace( '%service-category%/', 'service/', $permalink );
      }else{
        write_log('2');
        $post_terms = array();
        foreach ( $terms as $term )
          $post_terms[] = $term->slug;
        return str_replace( '%service-category%', implode( ',', $post_terms ) , $permalink );
      }
  }
    
}

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="btn btn-primary"';
}

?>