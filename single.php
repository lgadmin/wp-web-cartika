<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>
	<div id="primary">

		<?php

		$title = get_the_title();
    	$link = get_permalink();
    	$date = get_the_date();
    	$author = get_the_author_meta('display_name');

    	?>

		<section id="cartika-featureimage" class="py-5 bg-primary text-white" style="background-image:url(<?php echo $featureimage['url']; ?>)">
			<div class="container"> 
				<div class="row align-items-center justify-content-center">
					<div class="feature_image_half_content col text-center">
						<h1><?php echo $title; ?></h1>
						<div><i><?php echo $date; ?></i></div>
					</div>
				</div>
			</div>
		</section>

		<main id="content" role="main" class="site-content container py-5">

			<div class="sidebar-main py-4">
				<div class="sidebar-body">

					<div class="single-post">

			        	<div class="details">
			        		<div class="description">
			        			<?php the_content(); ?>
			        		</div>
			        	</div>

			        	<?php if(get_next_post() || get_previous_post()): ?>
				        	<div class="blog-nav">
								<?php if(get_next_post()): ?>
									<a href="<?php echo get_next_post()->guid; ?>" class="btn btn-blue previous">Previous Post</a>
								<?php endif; ?>
								<?php if(get_previous_post()): ?>
									<a href="<?php echo get_previous_post()->guid; ?>" class="btn btn-blue next">Next Post</a>
								<?php endif; ?>
							</div>
						<?php endif; ?>
			        </div>

				</div>
				
				<?php get_sidebar(); ?>
			</div>
		</main>
	</div>

<?php get_footer(); ?>