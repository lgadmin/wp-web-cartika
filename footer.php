  <?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<footer id="colophon" class="site-footer">
		
		<div id="site-footer" class="bg-night text-white clearfix">
			<div class="container">
				<div class="row justify-content-between pt-5 pb-4">
					<div class="site-footer-alpha col col-12 col-sm-auto">
						<div class="mb-4 px-3"><?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?></div>
						<?php dynamic_sidebar('footer-alpha'); ?>
					</div>
					<div class="site-footer-bravo col col-12 col-sm-12 col-md-12 col-lg-8"><?php dynamic_sidebar('footer-bravo'); ?></div>
				</div>
			</div>
		</div>

		<div id="site-legal" class="bg-dark text-white pt-3 clearfix">
			<div class="container">
				<div class="row px-3">
					<div class="site-info col col-12 col-sm-6 pb-3"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
					<div class="site-longevity col col-12 col-sm-6 pb-3"><?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
				</div>
			</div>
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
